var limit = 300,
    duration = 16,
    now = new Date(Date.now() - duration);

var width = 1400,
    height = 400,
    padding = 50;

var groups = {};

for (var i = 0; i < 12; i++) {
    groups[i] = {
        value: 0,
        color: getRandomColor(),
        data: d3.range(limit).map(function () {
            return 0;
        })
    };
}

var x = d3.time.scale()
    .domain([now - (limit - 2), now - duration])
    .range([0, width]);

var y = d3.scale.linear()
    .domain([0, 65536])
    .range([height, 0]);

var line = d3.svg.line()
    .interpolate('basis')
    .x(function (d, i) {
        return x(now - (limit - 1 - i) * duration)
    })
    .y(function (d) {
        return y(d)
    });


function zoomed() {
    container.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
}


var zoom = d3.behavior.zoom()
    .scaleExtent([1, 10])
    .on("zoom", zoomed);

var svg = d3.select('.graph').append('svg')
    .attr('class', 'chart')
    .attr('width', width)
    .attr('height', height + 50)
    .call(zoom);

var container = svg.append("g");

var axis = svg.append('g')
    .attr('class', 'x axis no-text')
    .attr('transform', 'translate(0,' + height + ')')
    .call(x.axis = d3.svg.axis().scale(x).orient('bottom'));


var yAxis = d3.svg.axis()
    .orient("left")
    .scale(y);

svg.append("g")
    .attr("class", "y axis")
    .attr("transform", "translate(" + padding + ",0)")
    .call(yAxis);

var paths = svg.append('g');

for (var name in groups) {
    var group = groups[name];
    group.path = paths.append('path')
        .data([group.data])
        .attr('class', name + ' group')
        .style('stroke', group.color)
}

function tick() {
    now = new Date();
    var result = JSON.parse(httpGet('/update'));
    // Add new values
    for (var name in groups) {
        $("#value_" + name).text(result[name]);
        var group = groups[name];
        group.data.push(result[name]);
        group.path.attr('d', line)
    }

    // Shift domain
    x.domain([now - (limit - 2) * duration, now - duration]);

    // Slide x-axis left
    axis.transition()
        .duration(duration)
        .ease('linear')
        .call(x.axis);


    // Slide paths left
    paths.attr('transform', null)
        .transition()
        .duration(duration)
        .ease('linear')
        .attr('transform', 'translate(' + x(now - (limit - 1) * duration) + ')')
        .each('end', tick);

    // Remove oldest data point from each group
    for (var name in groups) {
        var group = groups[name];
        group.data.shift()
    }
}

function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false);
    xmlHttp.send(null);
    if (xmlHttp.status == 500) {
        alert("Can't connect to serial port device");
        return;
    }
    return xmlHttp.responseText;
}
tick();

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

var show_line = function () {
    var id = $(this).attr('id');
    if (this.checked) {
        $("." + id).show();
    }
    else {
        $("." + id).hide();
    }
};

var rescale = function () {
    var lower_bound = $("#lower-bound").val();
    var upper_bound = $("#upper-bound").val();
    console.log(lower_bound);
    if (!(lower_bound >= 0 && lower_bound != "" && upper_bound <= 65536 && upper_bound != "")) {
        return;
    }
    y = d3.scale.linear()
        .domain([lower_bound, upper_bound])
        .range([height, 0]);
    yAxis = d3.svg.axis()
        .orient("left")
        .scale(y);
    svg.select(".y.axis").call(yAxis);
};

$(".checkbox").each(show_line);

$(".checkbox").change(show_line);

$("#rescale-button").click(rescale);