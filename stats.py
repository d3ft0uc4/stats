import random
import string
from json import dumps
import serial
import sys
from flask import Flask, render_template, make_response

app = Flask(__name__)

port = None
connections = []
byte = 24
test_mode = True


@app.route('/')
def hello_world():
    return render_template('stats.html', indexes=range(1, int(byte / 2)))


@app.route('/update')
def update():
    if test_mode:
        ans = random_string()
    else:
        if len(connections) == 0:
            connect()
        safeguards_count = 0
        ans = [0] * 24
        connections[0].flushInput()
        connections[0].flushOutput()
        while True:
            bte = connections[0].read(1)
            print (connections[0].inWaiting())
            if bte == 65 or bte == b'A':
                safeguards_count += 1
            else:
                safeguards_count = 0
            if safeguards_count == 4:
                ans = connections[0].read(byte)
                break

    data = [ans[i:i + 2] for i in range(0, len(ans), 2)]
    result = []
    for val in data:
        try:
            result.append(ord(val[0]) * 256 + ord(val[1]))
        except:
            result.append(val[0] * 256 + val[1])
    return make_response(dumps(result))


def connect():
    connection = serial.Serial(port, 115200,
                               parity=serial.PARITY_NONE,
                               stopbits=serial.STOPBITS_ONE,
                               timeout=0.3,
                               writeTimeout=0.1
                               )
    connections.append(connection)


def random_string():
    _str = ''
    for i in range(0, byte):
        _str += (random.choice(string.ascii_letters))
    return _str


def usage():
    print ("Usage: stats.py --port <port>")


if __name__ == '__main__':
    if len(sys.argv) != 3:
        usage()
    else:
        port = sys.argv[2]
        app.run(debug=True)
